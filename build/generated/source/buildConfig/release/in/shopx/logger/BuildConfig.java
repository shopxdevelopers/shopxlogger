/**
 * Automatically generated file. DO NOT MODIFY
 */
package in.shopx.logger;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "in.shopx.logger";
  /**
   * @deprecated APPLICATION_ID is misleading in libraries. For the library package name use LIBRARY_PACKAGE_NAME
   */
  @Deprecated
  public static final String APPLICATION_ID = "in.shopx.logger";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
