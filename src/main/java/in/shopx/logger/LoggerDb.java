package in.shopx.logger;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class LoggerDb extends SQLiteOpenHelper {

    private static final String DB_NAME = "shopxEventsDb";
    private static final int DB_VERSION = 1;
    private static final String TABLE_NAME = "logger";
    private static final String ID_COL = "id";
    private static final String JSON_STRING = "encodedJsonString";
    private static final String EVENT_NAME = "eventName";


    public LoggerDb(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_NAME + " ("
                + ID_COL + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + EVENT_NAME + " TEXT, "
                + JSON_STRING + " TEXT)";

        // at last we are calling a exec sql
        // method to execute above sql query
        db.execSQL(query);
    }

    public void addLog( String encodedJsonString,  String apiUrl,  HashMap<String, String> apiHeaders,
                        HashMap<String, String> apiFormData,  String eventName) {

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(JSON_STRING, encodedJsonString);
        values.put(EVENT_NAME, eventName);
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }

    public ArrayList<Events> readCourses() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursorCourses = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);


        ArrayList<Events> courseModalArrayList = new ArrayList<>();

        // moving our cursor to first position.
        if(cursorCourses!=null && cursorCourses.getCount() > 0) {
            try {
                if (cursorCourses.moveToFirst()) {
                    do {
                        courseModalArrayList.add(new Events(cursorCourses.getString(cursorCourses.getColumnIndex("eventName")),
                                cursorCourses.getString(cursorCourses.getColumnIndex("encodedJsonString"))));
                    } while (cursorCourses.moveToNext());

                }
            }catch (Exception e){

            }
        }
        cursorCourses.close();
	db.close();
        return courseModalArrayList;
    }

    public int deleteAllrecord() {
        int number = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        try {
            number = db.delete(TABLE_NAME, "1", null);
        } catch (Exception e) {

        }
	db.close();
        return number;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
