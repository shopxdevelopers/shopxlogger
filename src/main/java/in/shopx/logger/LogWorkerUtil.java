package in.shopx.logger;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.core.content.ContextCompat;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.Headers;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import okio.Buffer;

public class LogWorkerUtil {

    private static long PERIODIC_INTERVAL = 3 * 60 * 60 * 1000; // 3 hours
    private static String MAIN_WORKER_TAG = "Logger_Main_Worker";

    private static LogWorkerUtil instance;
    private Context context;
    private static boolean isTestBuild;
    private static ThreadPoolExecutor executor;
    private static LoggerDb loggerDb;

    public static LogWorkerUtil getInstance(Context context, boolean isTestBuild) {
        if (instance == null) {
            instance = new LogWorkerUtil(context);
        }
        LogWorkerUtil.isTestBuild = isTestBuild;
        return instance;
    }

    private LogWorkerUtil(Context context) {
        this.context = context;
        initExecutor();
        initLoggerDb(context);
    }

    public void startPeriodicUploadWork(String workerTag, long loggerFrequency, String apiUrl, String csvFileName, String zipFileName,
                                        HashMap<String, String> apiHeaders,
                                        HashMap<String, String> apiFormData) throws Exception {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            throw new SecurityException("Write permission not granted!!");
        }
        if (!csvFileName.contains(".")) {
            throw new IOException("Csv File name must contain file extension!!");
        }
        if (!zipFileName.contains(".")) {
            throw new IOException("Csv Zip File name must contain file extension!!");
        }

        if (loggerFrequency != 0) {
            PERIODIC_INTERVAL = loggerFrequency;
        }

        androidx.work.Data.Builder data = new androidx.work.Data.Builder();
        data.putString(AppConstants.InputKeys.KEY_API_URL, apiUrl);
        data.putString(AppConstants.InputKeys.KEY_CSV_NAME, csvFileName);
        data.putString(AppConstants.InputKeys.KEY_CSV_ZIP_NAME, zipFileName);
        data.putString(AppConstants.InputKeys.KEY_API_HEADER, apiHeaders == null ? "" : new Gson().toJson(apiHeaders));
        data.putString(AppConstants.InputKeys.KEY_API_FORM_DATA, apiFormData == null ? "" : new Gson().toJson(apiFormData));
        data.putBoolean(AppConstants.InputKeys.KEY_ENABLE_TEST_NOTIFICATION, isTestBuild);

        Constraints constraint = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
        PeriodicWorkRequest workRequest = new PeriodicWorkRequest.Builder(LogUploadWorker.class,
                PERIODIC_INTERVAL, TimeUnit.MILLISECONDS)
                .addTag(workerTag)
                .setInputData(data.build())
                .setConstraints(constraint)
                .build();
        WorkManager workManager = WorkManager.getInstance(context);
        workManager.enqueueUniquePeriodicWork(workerTag, ExistingPeriodicWorkPolicy.KEEP, workRequest);
    }

    public void startOneTimeUploadWork(String workerTag, String apiUrl, String csvFileName, String zipFileName,
                                       HashMap<String, String> apiHeaders,
                                       HashMap<String, String> apiFormData) throws Exception {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            throw new SecurityException("Write permission not granted!!");
        }
        if (!csvFileName.contains(".")) {
            throw new Exception("Csv File name must contain file extension!!");
        }
        if (!zipFileName.contains(".")) {
            throw new Exception("Csv Zip File name must contain file extension!!");
        }

        androidx.work.Data.Builder data = new androidx.work.Data.Builder();
        data.putString(AppConstants.InputKeys.KEY_API_URL, apiUrl);
        data.putString(AppConstants.InputKeys.KEY_CSV_NAME, csvFileName);
        data.putString(AppConstants.InputKeys.KEY_CSV_ZIP_NAME, zipFileName);
        data.putString(AppConstants.InputKeys.KEY_API_HEADER, apiHeaders == null ? "" : new Gson().toJson(apiHeaders));
        data.putString(AppConstants.InputKeys.KEY_API_FORM_DATA, apiFormData == null ? "" : new Gson().toJson(apiFormData));
        data.putBoolean(AppConstants.InputKeys.KEY_ENABLE_TEST_NOTIFICATION, isTestBuild);

        Constraints constraint = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build();
        OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(LogUploadWorker.class)
                .addTag(workerTag)
                .setInputData(data.build())
                .setConstraints(constraint)
                .build();
        WorkManager workManager = WorkManager.getInstance(context);
        workManager.enqueueUniqueWork(workerTag, ExistingWorkPolicy.KEEP, workRequest);
    }

    public long getPeriodicIntervalForLogs() {
        return PERIODIC_INTERVAL;
    }

    public void reinitializePeriodicUploadWorker(String workerTag, long loggerFrequency, String apiUrl, String csvFileName,
                                                 String zipFileName, HashMap<String, String> apiHeaders,
                                                 HashMap<String, String> apiFormData) throws Exception {
        WorkManager.getInstance(context).cancelAllWorkByTag(workerTag);
        startPeriodicUploadWork(workerTag, loggerFrequency, apiUrl, csvFileName, zipFileName, apiHeaders, apiFormData);
    }

    public boolean saveToCsv(String fileName, JsonObject jsonObject) {
        if (context == null)
            return false;

        File file = new File(context.getFilesDir(), fileName);

        CSVReader reader = null;
        CSVWriter writer = null;

        try {
            // If file already exists, then that means previously data was
            // saved in the file. In that case, we have to extract all the data
            // from the file.
            List<String[]> prevContent = null;
            if (file.exists()) {
                reader = new CSVReader(new FileReader(file));
                prevContent = reader.readAll();
            }

            writer = new CSVWriter(new FileWriter(file), ',');
            if (reader == null)
                reader = new CSVReader(new FileReader(file));

            // If this is a new file and no data is there then
            // write the headers content in the file
            // Else, write the previously written data
            if (prevContent == null) {
                writer.writeNext(jsonObject.keySet().toArray(new String[jsonObject.keySet().size()]));
            } else {
                writer.writeAll(prevContent);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (writer == null || reader == null || !file.exists())
            return false;

        String[] row = new String[jsonObject.keySet().size()];
        Iterator<String> keyIterator = jsonObject.keySet().iterator();
        int pos = 0;
        while (keyIterator.hasNext()) {
            String value = jsonObject.get(keyIterator.next()).getAsString();
            row[pos] = value;
            pos++;
        }
        writer.writeNext(row);
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    public void sendEvents(String encodedJsonString, String apiUrl, HashMap<String, String> apiHeaders,
                           HashMap<String, String> apiFormData, String eventName, AppConstants.EVENT_PUSH_MECHANISM pushMechanismType) throws Exception {

        switch (pushMechanismType) {
            case SINGLE_EVENT:
                sendSingleEvent(encodedJsonString, apiUrl, apiHeaders, apiFormData, eventName);
                Log.e("SINGLE_EVENT", eventName);
                break;
            case MULTIPLE_EVENT:
                sendMultipleEvents(encodedJsonString, apiUrl, apiHeaders, apiFormData, eventName);
                Log.e("MULTIPLE_EVENT", eventName);
                break;
            default:
                throw new Exception("Invalid Push Mechanism Type");
        }
    }

    public void sendSingleEvent(String encodedJsonString, String apiUrl, HashMap<String, String> apiHeaders,
                                HashMap<String, String> apiFormData, String eventName) {
        executor.submit(() -> sendInstantly(encodedJsonString, apiUrl, apiHeaders, apiFormData, eventName));

    }

    public void sendMultipleEvents(String encodedJsonString, String apiUrl, HashMap<String, String> apiHeaders,
                                   HashMap<String, String> apiFormData, String eventName) {
        executor.submit(() -> persistAndPush(encodedJsonString, apiUrl, apiHeaders, apiFormData, eventName));
    }

    public void sendInstantly(String encodedJsonString, String apiUrl, HashMap<String, String> apiHeaders,
                              HashMap<String, String> apiFormData, String eventName) {

        MultipartBuilder multipartBuilder = buildApiFormData(apiFormData);

        RequestBody requestBody = multipartBuilder
                .addFormDataPart("eventName", eventName)
                .addFormDataPart("eventData", encodedJsonString)
                .build();

        Request.Builder requestBuilder = new Request.Builder();

        Request request = requestBuilder
                .url(apiUrl)
                .headers(getHeaders(apiHeaders))
                .post(requestBody)
                .build();

        flush(request);
    }

    public void persistAndPush(final String encodedJsonString, final String apiUrl, final HashMap<String, String> apiHeaders,
                               final HashMap<String, String> apiFormData, final String eventName) {
        loggerDb.addLog(encodedJsonString, apiUrl, apiHeaders, apiFormData, eventName);
        checkAndPush(apiFormData, apiHeaders, apiUrl);
    }


    private synchronized void checkAndPush(HashMap<String, String> apiFormData, HashMap<String, String> apiHeaders, String apiUrl) {

        int count = loggerDb.numberOfRows();
        if (count >= 10) {
            Request request = createRequestFromStorage(apiFormData, apiHeaders, apiUrl);
            flush(request);
            loggerDb.deleteAllrecord();
        }
    }

    public void stopUploadWorker(String tag) {
        WorkManager.getInstance(context).cancelAllWorkByTag(tag);
    }

    private static String bodyToString(final Request request) {

        try {
            final Request copy = request.newBuilder().build();
            final Buffer buffer = new Buffer();
            copy.body().writeTo(buffer);
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }

    private static Headers getHeaders(HashMap<String, String> apiHeaders) {
        return Headers.of(apiHeaders);
    }

    private static MultipartBuilder buildApiFormData(final HashMap<String, String> apiFormData) {
        MultipartBuilder multipartBuilder = new MultipartBuilder()
                .type(MultipartBuilder.FORM);
        if (apiFormData != null) {
            Iterator it = apiFormData.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                multipartBuilder.addFormDataPart(pair.getKey().toString(), pair.getValue().toString());
                it.remove();
            }
        }
        return multipartBuilder;
    }

    private static void flush(Request request) {
        if (request == null) {
            return;
        }
        try {
            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(2, TimeUnit.SECONDS); // connect timeout
            client.setWriteTimeout(1, TimeUnit.SECONDS); // write timeout
            client.setReadTimeout(1, TimeUnit.SECONDS); // socket timeout
            client.newCall(request)
                    .enqueue(new Callback() {
                        @Override
                        public void onFailure(Request request, IOException e) {
                            Log.e("Flush Failed", e.toString());
                        }

                        @Override
                        public void onResponse(Response response) throws IOException {
                            Log.e("Flush Success", response.toString());
                        }

                    });
        } catch (Exception e) {

        }
    }

    private static ThreadPoolExecutor initExecutor() {
        executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        executor.setCorePoolSize(5);
        executor.setMaximumPoolSize(10);
        return executor;
    }

    private static LoggerDb initLoggerDb(Context context) {
        loggerDb = new LoggerDb(context);
        return loggerDb;
    }

    private static Request createRequestFromStorage(HashMap<String, String> apiFormData, HashMap<String, String> apiHeaders, String apiUrl) {
        try {
            ArrayList<Events> cm = loggerDb.readCourses(); //Storing data to cache from db
            String publisher = apiFormData.get("publisher");
            String userId = apiFormData.get("userId");
            Data data = new Data(publisher, userId, cm);

            //Convert to JSON String
            Gson gson = new GsonBuilder().create();
            String eventsJsonStr = gson.toJsonTree(data).toString();

            Request.Builder requestBuilder = new Request.Builder();
            RequestBody requestBody = RequestBody.create(
                    MediaType.parse("application/json; charset=utf-8"), eventsJsonStr);

            Log.e("DB DATA", eventsJsonStr);

            Request request = requestBuilder
                    .url(apiUrl)
                    .headers(getHeaders(apiHeaders))
                    .post(requestBody)
                    .build();

            return request;
        } catch (Exception e) {
            return null;
        }

    }

}