package in.shopx.logger;

import java.util.ArrayList;

public class Data {

    private String publisher;
    private String userId;
    private ArrayList<Events> events;

    public Data(String publisher, String userId, ArrayList<Events> eventData) {
        this.publisher = publisher;
        this.userId = userId;
        this.events = eventData;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public ArrayList<Events> getEventData() {
        return events;
    }

    public void setEventData(ArrayList<Events> eventData) {
        this.events = eventData;
    }


}
