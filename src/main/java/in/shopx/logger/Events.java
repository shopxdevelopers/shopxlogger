package in.shopx.logger;

public class Events {

    private String eventName;
    private String eventData;

    public String getEncodedJsonString() {
        return eventData;
    }

    public void setEncodedJsonString(String encodedJsonString) {
        this.eventData = encodedJsonString;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }


    // constructor
    public Events(String eventName, String encodedJsonString) {
        this.eventName = eventName;
        this.eventData = encodedJsonString;

    }

}
