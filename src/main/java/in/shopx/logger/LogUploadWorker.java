package in.shopx.logger;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Call;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.ListenableWorker;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import static android.content.Context.NOTIFICATION_SERVICE;

public class LogUploadWorker extends Worker {

    private String fileCrc = "";
    private String csv_file_name = "ShopXLog.csv";
    private String csv_zip_file_name = "ShopXLog.zip";
    private String apiUrl = "";
    private HashMap<String, String> headers;
    private HashMap<String, String> formData;
    private boolean isTestBuild = false;

    public LogUploadWorker(@NonNull Context context, @NonNull WorkerParameters params) {
        super(context, params);

        if (params.getInputData().hasKeyWithValueOfType(AppConstants.InputKeys.KEY_API_URL, String.class))
            apiUrl = params.getInputData().getString(AppConstants.InputKeys.KEY_API_URL);

        if (params.getInputData().hasKeyWithValueOfType(AppConstants.InputKeys.KEY_CSV_NAME, String.class))
            csv_file_name = params.getInputData().getString(AppConstants.InputKeys.KEY_CSV_NAME);

        if (params.getInputData().hasKeyWithValueOfType(AppConstants.InputKeys.KEY_CSV_ZIP_NAME, String.class))
            csv_zip_file_name = params.getInputData().getString(AppConstants.InputKeys.KEY_CSV_ZIP_NAME);

        if (params.getInputData().hasKeyWithValueOfType(AppConstants.InputKeys.KEY_ENABLE_TEST_NOTIFICATION, Boolean.class))
            isTestBuild = params.getInputData()
                    .getBoolean(AppConstants.InputKeys.KEY_ENABLE_TEST_NOTIFICATION, false);

        if (params.getInputData().hasKeyWithValueOfType(AppConstants.InputKeys.KEY_API_HEADER, String.class)) {
            String json = params.getInputData().getString(AppConstants.InputKeys.KEY_API_HEADER);
            if (!TextUtils.isEmpty(json)) {
                Type type = new TypeToken<HashMap<String, String>>() {
                }.getType();
                headers = new Gson().fromJson(json, type);
            }
        }

        if (params.getInputData().hasKeyWithValueOfType(AppConstants.InputKeys.KEY_API_FORM_DATA, String.class)) {
            String json = params.getInputData().getString(AppConstants.InputKeys.KEY_API_FORM_DATA);
            if (!TextUtils.isEmpty(json)) {
                Type type = new TypeToken<HashMap<String, String>>() {
                }.getType();
                formData = new Gson().fromJson(json, type);
            }
        }
    }

    @NonNull
    @Override
    public Result doWork() {
        displayTestNotification("Event Worker Success");
        return uploadEventLogs();
    }

    private Result uploadEventLogs() {
        fileCrc = "";
        if (zipFile()) {
            File zippedFile = new File(getApplicationContext().getFilesDir(), csv_zip_file_name);

            final MediaType MEDIA_TYPE = MediaType.parse("application/zip");
            MultipartBuilder multipartBuilder = new MultipartBuilder()
                    .type(MultipartBuilder.FORM);
            if (formData != null) {
                Iterator it = formData.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    multipartBuilder.addFormDataPart(pair.getKey().toString(), pair.getValue().toString());
                    it.remove();
                }
            }
            RequestBody requestBody = multipartBuilder
                    .addFormDataPart("logStartTime", Util.millisToDateString(zippedFile.lastModified(), "yyyy-MM-dd HH:mm:ss"))
                    .addFormDataPart("logEndTime", Util.millisToDateString(zippedFile.lastModified(), "yyyy-MM-dd HH:mm:ss"))
                    .addFormDataPart("logCrc", fileCrc)
                    .addFormDataPart("fileCrc", fileCrc)
                    .addFormDataPart("logFileName", csv_zip_file_name)
                    .addFormDataPart("compression", csv_zip_file_name.substring(csv_zip_file_name.lastIndexOf(".")))
                    .addFormDataPart("file", zippedFile.getName().substring(0, zippedFile.getName().lastIndexOf(".")), RequestBody.create(MEDIA_TYPE, zippedFile))
                    .build();

            Request.Builder requestBuilder = new Request.Builder();
            if (headers != null) {
                Iterator it = headers.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry) it.next();
                    requestBuilder.header(pair.getKey().toString(), pair.getValue().toString());
                    it.remove();
                }
            }

            Request request = requestBuilder
                    .url(apiUrl)
                    .post(requestBody)
                    .build();

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(30, TimeUnit.SECONDS); // connect timeout
            client.setWriteTimeout(30, TimeUnit.SECONDS); // write timeout
            client.setReadTimeout(30, TimeUnit.SECONDS); // socket timeout
            Call call = client.newCall(request);
            try {
                Response response = call.execute();
                if (response.code() == 200) {
                    displayTestNotification("API Success : " + apiUrl);
                    zippedFile.delete();
                    return ListenableWorker.Result.success();
                } else {
                    displayTestNotification("API Failure---" + apiUrl + " --- Response :" + response.code() + "::" + response.message());
                    return ListenableWorker.Result.retry();
                }
            } catch (IOException e) {
                e.printStackTrace();
                displayTestNotification("Failure due to " + e.getCause() + " : " + apiUrl);
                return ListenableWorker.Result.retry();
            }
        } else {
            displayTestNotification("No Events present : " + apiUrl);
            return Result.success();
        }
    }

    private boolean zipFile() {
        final int BUFFER = 2048;
        try {
            File file = new File(getApplicationContext().getFilesDir(), csv_file_name);
            File zippedFile = new File(getApplicationContext().getFilesDir(), csv_zip_file_name);

            // Zipped file is already in the file system
            // then upload this file
            if (zippedFile.exists()) {
                calculateFileCrc();
                return true;
            }

            if (!file.exists()) {
                return false;
            }

            BufferedInputStream origin = null;
            FileOutputStream dest = new FileOutputStream(zippedFile);
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
            byte[] data = new byte[BUFFER];
            FileInputStream fi = new FileInputStream(file);
            origin = new BufferedInputStream(fi, BUFFER);
            ZipEntry entry = new ZipEntry(getLastPathComponent(file.getAbsolutePath()));
            out.putNextEntry(entry);
            CRC32 zipEntryCrc = new CRC32();
            zipEntryCrc.update(data);
            int count;
            while ((count = origin.read(data, 0, BUFFER)) != -1) {
                out.write(data, 0, count);
            }
            out.close();
            fileCrc = String.valueOf(zipEntryCrc.getValue());

            // Update zipped file date as per the original file
            // before we delete the original file
            zippedFile.setLastModified(file.lastModified());

            if (file.exists())
                file.delete();

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void calculateFileCrc() {
        ZipFile zipFile = null;
        try {
            // open a zip file for reading
            File zippedFile = new File(getApplicationContext().getFilesDir(), csv_zip_file_name);

            zipFile = new ZipFile(zippedFile);

            // get an enumeration of the ZIP file entries
            Enumeration<? extends ZipEntry> e = zipFile.entries();

            while (e.hasMoreElements()) {
                ZipEntry entry = e.nextElement();
                // get the name of the entry
                String entryName = entry.getName();
                // get the CRC-32 checksum of the uncompressed entry data, or -1 if not known
                long crc = entry.getCrc();
                fileCrc = String.valueOf(crc);
            }

        } catch (IOException ioe) {
            System.out.println("Error opening zip file" + ioe);
        } finally {
            try {
                if (zipFile != null) {
                    zipFile.close();
                }
            } catch (IOException ioe) {
                System.out.println("Error while closing zip file" + ioe);
            }
        }

    }

    private String getLastPathComponent(String filePath) {
        String[] segments = filePath.split("/");
        if (segments.length == 0)
            return "";
        return segments[segments.length - 1];
    }

    private void displayTestNotification(String msg) {
//        if (!isTestBuild)
//            return;
//
//        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(NOTIFICATION_SERVICE);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            NotificationChannel channel = new NotificationChannel("ShopX", "ShopX", NotificationManager.IMPORTANCE_DEFAULT);
//            assert notificationManager != null;
//            notificationManager.createNotificationChannel(channel);
//        }
//
//        NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), "ShopX")
//                .setContentTitle("ShopX Logger")
//                .setContentText(msg)
//                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
//                .setSmallIcon(R.drawable.ic_notification_logo)
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
//
//        assert notificationManager != null;
//        notificationManager.notify(new AtomicInteger(0).incrementAndGet(), builder.build());

    }

}
