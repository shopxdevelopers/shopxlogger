package in.shopx.logger;

public class AppConstants {

    public static final String ENV_PROD = "ENV_PROD";
    public static final String ENV_DEBUG = "ENV_DEBUG";

    public enum EVENT_PUSH_MECHANISM{
        SINGLE_EVENT,
        MULTIPLE_EVENT
    }

    public class InputKeys {
        public static final String KEY_CSV_NAME = "key_csv_name";
        public static final String KEY_CSV_ZIP_NAME = "key_zipped_csv_name";
        public static final String KEY_ENABLE_TEST_NOTIFICATION = "key_enable_test_notif";
        public static final String KEY_API_URL = "key_api_url";
        public static final String KEY_API_HEADER = "key_api_header";
        public static final String KEY_API_FORM_DATA = "key_api_form_data";




    }

    

}
