package in.shopx.logger;

public class CourseModel {

    private String encodedJsonString;
    private String apiUrl;
    private String apiHeaders;
    private String apiFormData;
    private String eventName;
    private int id;

    public String getEncodedJsonString() {
        return encodedJsonString;
    }

    public void setEncodedJsonString(String encodedJsonString) {
        this.encodedJsonString = encodedJsonString;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getApiHeaders() {
        return apiHeaders;
    }

    public void setApiHeaders(String apiHeaders) {
        this.apiHeaders = apiHeaders;
    }

    public String getApiFormData() {
        return apiFormData;
    }

    public void setApiFormData(String apiFormData) {
        this.apiFormData = apiFormData;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // constructor
    public CourseModel(String encodedJsonString, String apiUrl, String apiHeaders, String apiFormData, String eventName) {
        this.encodedJsonString = encodedJsonString;
        this.apiUrl = apiUrl;
        this.apiHeaders = apiHeaders;
        this.apiFormData = apiFormData;
        this.eventName = eventName;
    }

}
